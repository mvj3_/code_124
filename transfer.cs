                byte[] bytes = ...
                Stream stream = new MemoryStream(bytes);
                BitmapImage image = new BitmapImage();

                image.SetSourceAsync(new MemoryRandomAccessStream(stream));